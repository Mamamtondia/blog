---
title: Example 2 Article
date: 2020-05-27
tags: example, exploration
---

This is my second example article. This blog has been generated using [middleman](https://middlemanapp.com/basics/blogging/).

![My Photo](/blog/images/middleman.png)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

